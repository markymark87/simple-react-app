# 1.0.0 (2020-04-14)


### Bug Fixes

* add before script entries back in ([f2de5a9](https://gitlab.com/markymark87/simple-react-app/commit/f2de5a9a22d4ce5eb5a45c3bac27998b1b24f5e2))
* delete dist ([f8a1f85](https://gitlab.com/markymark87/simple-react-app/commit/f8a1f85b9601e44843adcec774578086e92e2f87))
* remove keyscan ([b6e281c](https://gitlab.com/markymark87/simple-react-app/commit/b6e281c1c17b7bd48cffca10b7fa7c68490af483))
* rename CI file ([36fcbbb](https://gitlab.com/markymark87/simple-react-app/commit/36fcbbbebfeade1d987f80d4589616960731e6b9))
* set repositiry url ([97e3942](https://gitlab.com/markymark87/simple-react-app/commit/97e3942a064252ec4f6f53654261f462714abe60))
* update CI ([4b7f731](https://gitlab.com/markymark87/simple-react-app/commit/4b7f73177b0e881df0c1b5b36f361334f3554084))
* update CI ([00a896d](https://gitlab.com/markymark87/simple-react-app/commit/00a896ddc6a444bc6e72094c07e203465f04ffbc))
* update gitignore ([29e8459](https://gitlab.com/markymark87/simple-react-app/commit/29e845909b89302325c7b2d25151e4f0ff559af7))
