module.exports = {
	ignore: [/[\/\\]core-js/, /@babel[\/\\]runtime/],
	presets: [
		"@babel/preset-env",
		"@babel/preset-react"
	],
	plugins: [
		[
			"@babel/plugin-proposal-class-properties",
			{
				loose: true
			}
		],
		[
			"@babel/plugin-transform-runtime",
			{
				regenerator: true
			}
		],
		"@babel/plugin-proposal-object-rest-spread",
		"@babel/plugin-proposal-optional-chaining",
		"@babel/plugin-proposal-nullish-coalescing-operator",
		"@babel/plugin-transform-async-to-generator",
		"@babel/plugin-syntax-class-properties",
		"@babel/plugin-syntax-object-rest-spread",
		"@babel/plugin-transform-react-inline-elements",
		"@babel/plugin-transform-react-constant-elements",
		"@babel/plugin-syntax-dynamic-import"
	]
};
